const {db, getLastOrder} = require("../db/conexion");

const mediaModel =  {
// listado de media
    async getAll (req, res) {
        let sql = `
            SELECT media.*, tipoMedia.nombre as tipoMediaNombre, integrantes.nombre as integranteNombre, integrantes.apellido as integranteApellido
            FROM media
            LEFT JOIN tipoMedia ON media.tipoMedia = tipoMedia.id
            LEFT JOIN integrantes ON media.matricula = integrantes.matricula
            WHERE media.activo = 1
        `;
        if (req.query["s"]) {
            if (req.query['s']['integrante']) {
                sql += ` AND integrantes.id LIKE '%${req.query['s']['integrante']}%'`;
            }

            if (req.query['s']['tipoMedia']) {
                sql += ` AND tipoMedia.id LIKE '%${req.query['s']['tipoMedia']}%'`;
            }
        }
        sql += " ORDER BY orden";
        return new Promise((resolve, reject) => {
            db.all(sql, (error, media) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media);
                }
            });
        });
    },


// formulario de edición
    getById(id) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM media WHERE id = ?`, [id], (error, media) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media || null);
                }
            });
        });
    },


// formulario de creación
    async create(req, res) {
        const lastOrder = await getLastOrder("media");
        const newOrder = lastOrder + 1;

        return new Promise((resolve, reject) => {
            db.run(`insert into media (src, url, titulo, tipoMedia, matricula, activo, orden) 
                select ?, ?, ?, ?, integrantes.matricula, ?, ?
                from integrantes where integrantes.id = ?`,
                [
                    req.srcPath,
                    req.url,
                    req.titulo,
                    req.tipoMedia,
                    req.activo,
                    newOrder,
                    req.integrante
                ], (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                });
        });
    },


// formulario de actualización
    update(req, id, src) {
        return new Promise((resolve, reject) => {
            db.run(`UPDATE media SET src = ?, url = ?, titulo = ?, tipoMedia = ?, matricula = ? WHERE id = ?`,
                [
                    src,
                    req.url,
                    req.titulo,
                    req.tipoMedia,
                    req.integrante,
                    id
                ], (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                });
        });
    },


// Borrar registro de media
    delete(id) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE media SET activo = 0 WHERE id = ?", [id], error => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

    getMediaByMatricula: function(matricula) {
        return new Promise((resolve, reject) => {
            db.all(`select * from media where activo = 1 and matricula = ? order by orden`, [matricula], (error, media) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(media);
                }
            });
        });
    },
};
module.exports = mediaModel;
