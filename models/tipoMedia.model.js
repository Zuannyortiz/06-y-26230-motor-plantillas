const {db, getLastOrder, run, getAll} = require("../db/conexion");

const tipoMediaModel =  {
// listado de tipo media
    async getAll (req, res) {
        let sql = "SELECT * FROM tipoMedia WHERE activo = 1";
        // console.log("filtro busqueda", req.query);
        for (const prop in req.query["s"]) {
            if (req.query["s"][prop]) {
                // console.log("prop", prop, req.query["s"][prop]);
                sql += ` AND ${prop} LIKE '%${req.query["s"][prop]}%'`;

            }
        }
        sql += " ORDER BY orden";
        return new Promise((resolve, reject) => {
            db.all(sql, (error, tipoMedia) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(tipoMedia);
                }
            });
        });
    },



// formulario de edición
    getById(id) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM tipoMedia WHERE id = ?`, [id], (error, tipoMedia) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(tipoMedia);
                }
            });
        });
    },

// formulario de creación
    async create(req, res) {
        const lastOrder = await getLastOrder("tipoMedia");
        const newOrden = lastOrder + 1;

        return new Promise((resolve, reject) => {
            db.run(`INSERT INTO tipoMedia (orden, id, nombre, activo)
                       VALUES (?, ?, ?, ?)`,
                [
                    newOrden,
                    req.id,
                    req.nombre,
                    req.activo
                ], (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                });
        });
    },

// formulario de actualización
    update(req, id) {
        return new Promise((resolve, reject) => {
            db.run(`UPDATE tipoMedia SET nombre = ? WHERE id = ?`,
                [
                    req.nombre,
                    id
                ], (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                });
        });
    },

// Borrar registro de tipo media
    delete(id) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE tipoMedia SET activo = 0 WHERE id = ?", [id], error => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },

    getAllTipoMedia: function() {
        return new Promise((resolve, reject) => {
            db.all("select * from tipoMedia where activo = 1 order by orden", (error, tipoMedia) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(tipoMedia);
                }
            });
        });
    },
};
module.exports = tipoMediaModel;
