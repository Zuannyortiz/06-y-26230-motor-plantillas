const {db, getLastOrder} = require("../db/conexion");

const integranteModel =  {
    // listado de integrantes
    async getAll (req, res) {
        let sql = "SELECT * FROM integrantes WHERE activo = 1";
        // console.log("filtro busqueda", req.query);
        for (const prop in req.query["s"]) {
            if (req.query["s"][prop]) {
                // console.log("prop", prop, req.query["s"][prop]);
                sql += ` AND ${prop} LIKE '%${req.query["s"][prop]}%'`;
            }
        }
        sql += " ORDER BY orden";
        return new Promise((resolve, reject) => {
            db.all(sql, (error, integrantes) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(integrantes);
                }
            });
        });
    },

// formulario de edición
    getById(id) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT * FROM integrantes WHERE id = ?`, [id], (error, integrantes) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(integrantes);
                }
            });
        });
    },

// formulario de creación
    async create(req, res) {
        const lastOrder = await getLastOrder("integrantes");
        const newOrder = lastOrder + 1;

        return new Promise((resolve, reject) => {
            db.run(`INSERT INTO integrantes (orden, nombre, apellido, matricula, pagina, activo)
                       VALUES (?, ?, ?, ?, ?, ?)`,
                [
                    newOrder,
                    req.nombre,
                    req.apellido,
                    req.matricula,
                    req.pagina,
                    req.activo
                ], (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                });
        });
    },

// formulario de actualización
    update(req, id) {
        return new Promise((resolve, reject) => {
            db.run(`UPDATE integrantes SET nombre = ?, apellido = ? WHERE id = ?`,
                [
                    req.nombre,
                    req.apellido,
                    id
                ], (error) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve();
                    }
                });
        });
    },


// Borrar registro de integrante
    delete(matricula) {
        return new Promise((resolve, reject) => {
            db.run("UPDATE integrantes SET activo = 0 WHERE matricula = ?", [matricula], error => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    },


    getByField: function(tabla, clave, valor) {
        return new Promise((resolve, reject) => {
            db.get(`SELECT 1 FROM ${tabla} WHERE ${clave} = ?`, [valor], (error, row) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(!!row);
                }
            });
        });
    },

    getAllIntegrantes: function() {
        return new Promise((resolve, reject) => {
            db.all("SELECT * FROM integrantes WHERE activo = 1", (error, integrantes) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(integrantes);
                }
            });
        });
    },

    getAlltMatriculas: function() {
        return new Promise((resolve, reject) => {
            db.all("select matricula from integrantes where activo = 1 order by orden", (error, matriculas) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(matriculas);
                }
            });
        });
    },

    getByMatricula: function(matricula) {
        return new Promise((resolve, reject) => {
            db.all(`select * from integrantes where activo = 1 and matricula = ? order by orden`, [matricula], (error, integrante) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(integrante);
                }
            });
        });
    }

};
module.exports = integranteModel;
