/*
index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro
*/

const tipoMediaStoreSchema = require("../../validators/tipoMedia/crearTipoMedia");
const editarTipoMediaStoreSchema = require("../../validators/tipoMedia/editarTipoMedia");
const { asociacionTipoMedia} = require("../../db/conexion");
const tipoMediaModel = require("../../models/tipoMedia.model");
const TipoMediaControllers = {

    //Listado de integrantes de /admin/tipoMedia

    index: async function (req, res) {
        try {
            const tipoMedia = await tipoMediaModel.getAll(req, res);
            res.render('admin/tipoMedia/index', {
                tipoMedia: tipoMedia
            });
        } catch (error) {
            console.error('Error al obtener los tipo media:', error);
            res.status(500).send('Error al obtener tipo media');
        }

    },

    create: async function (req, res) {
        res.render("admin/tipoMedia/crearTipoMedia");
    },


    store: async function (req,res) {
        const {error} = editarTipoMediaStoreSchema.validate(req.body);
        if (error) {
            const mensaje = error.details.map (detail => detail.message);
            const mensajeFormateado = mensaje.join('\n');
            return res.status(400).json({error:mensajeFormateado});
        }else{
            try {
                await tipoMediaModel.create(req.body);
                res.json({ success: true });
            } catch (error) {
                console.error('Error al crear tipo media:', error);
                res.status(500).json({ error: 'Error al crear tipo media' });
            }
        }

    },


    update: async function (req, res) {
        const id = parseInt(req.params.id);
        const {error} = tipoMediaStoreSchema.validate(req.body);
        if (error) {
            const mensaje = error.details.map (detail => detail.message);
            const mensajeFormateado = mensaje.join('\n');
            return res.status(400).json({error:mensajeFormateado});
        }else{
            try {
                await tipoMediaModel.update(req.body, id);
                res.json({success: true, message: 'Registro actualizado correctamente.'});
            } catch (error) {
                console.error('Error al actualizar el integrante:', error);
                res.status(500).json({error: '¡Error al actualizar al integrante!'});
            }
        }
    },


    edit: async function (req, res) {
        const id = parseInt(req.params.id);
        try {
            const tipoMedia = await tipoMediaModel.getById(id);
            res.render('admin/tipoMedia/tipoMediaEditform', {
                tipoMedia: tipoMedia
            });
        } catch (error) {
            console.error('Error al obtener el tipo media:', error);
            res.status(500).send('Error al obtener el tipo media');
        }
    },

    destroy: async function (req, res) {
        const id = parseInt(req.params.id);
        if (await asociacionTipoMedia(id)) {
            res.redirect("/admin/tipoMedia/listar?error=" + encodeURIComponent('¡No se puede eliminar el resgistro, esta información ya esta siendo utilizada en otras entidades!'));
        } else {
            try {
                await tipoMediaModel.delete(id);
                res.redirect("/admin/tipoMedia/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
            } catch (error) {
                console.error('Error al eliminar el Tipo Media:', error);
                res.redirect("/admin/tipoMedia/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
        }

    },

};

module.exports = TipoMediaControllers;