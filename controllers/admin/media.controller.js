/*
index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro
*/
const mediaStoreSchema = require("../../validators/media/crearMedia");
const editarMediaStoreSchema = require("../../validators/media/editarMedia");
const {getAll, getLastOrder, run, getOne} = require("../../db/conexion");
const fs = require("fs").promises;
const mediaModel = require("../../models/media.model");
const MediaControllers = {

    //Listado de media de /admin/media
    index: async function (req, res) {
        try {
            const integrantes = await getAll("SELECT * FROM integrantes WHERE activo = 1 ORDER BY nombre");
            const tipoMedia = await getAll("SELECT * FROM tipoMedia WHERE activo = 1 ORDER BY nombre");
            const media = await mediaModel.getAll(req, res);
            res.render("admin/media/index", {
                media: media,
                integrantes: integrantes,
                tipoMedia: tipoMedia,
            });
        } catch (error) {
            console.error('Error al obtener los datos de media:', error);
            res.status(500).send('Error al obtener los datos de media');
        }
    },

    create: async function (req, res) {
        try {
            const integrantes = await getAll("SELECT * FROM integrantes WHERE activo = 1 ORDER BY nombre");
            const tipoMedia = await getAll("SELECT * FROM tipoMedia WHERE activo = 1 ORDER BY nombre");
            res.render("admin/media/crearMedia", {
                integrantes: integrantes,
                tipoMedia: tipoMedia,
            });
        } catch (error) {
            console.error('Error al obtener datos para crear media:', error);
            res.status(500).send('Error al obtener datos para crear media');
        }
    },


    store: async function (req, res) {
        let srcPath = "";
        const errors = [];
        const { error } = mediaStoreSchema.validate(req.body);

        if (error) {
            const mensaje = error.details.map(detail => detail.message);
            const mensajeFormateado = mensaje.join('\n');
            return res.redirect(`/admin/media/crear?errors=${encodeURIComponent(JSON.stringify(mensajeFormateado))}`);
        } else {
            if (req.file) {
                const destino = `public/Images/${req.file.originalname}`;
                try {
                    await fs.rename(req.file.path, destino);
                    srcPath = `/Images/${req.file.originalname}`;
                } catch (err) {
                    console.error(err);
                    errors.push('¡Error al subir la imagen!');
                }
            }
            try {
                const { activo, ...campos } = req.body;
                const isActive = activo === 'on' ? 1 : 0;
                const nuevoReq = { ...campos, activo: isActive, srcPath };

                await mediaModel.create(nuevoReq);
                res.redirect(`/admin/media/listar?success=${encodeURIComponent('¡Registro insertado correctamente!')}`);
            } catch (err) {
                console.error(err);
                res.redirect(`/admin/media/crear?errors=${encodeURIComponent('¡Error al insertar el registro!')}`);
            }
        }
        if (errors.length > 0) {
            return res.redirect(`/admin/media/crear?errors=${encodeURIComponent(JSON.stringify(errors))}`);
        }
    },

    update: async function (req, res) {
        const id = parseInt(req.params.id);
        const {error, value} = editarMediaStoreSchema.validate(req.body);
        if (error) {
            const mensaje = error.details.map(detail => detail.message);
            const mensajeFormateado = mensaje.join('\n');
            return res.status(400).json({error: mensajeFormateado});
        } else {
            const mediaExistente = await getOne("SELECT * FROM media WHERE id = ?", [id]);
            try {
                let src = '';
                if (mediaExistente.src) {
                    src = mediaExistente.src;
                }
                if (req.file) {
                    var tpm_path = req.file.path;
                    var destino = "public/Images/" + req.file.originalname;
                    try {
                        await fs.rename(tpm_path, destino);
                        src = "/Images/" + req.file.originalname;
                    } catch (err) {
                        return res.sendStatus(500);
                    }
                }
                await mediaModel.update(req.body, id, src); // Pasa src como parámetro
                res.json({success: true, message: 'Registro actualizado correctamente.'});
            } catch (error) {
                console.error('Error al actualizar el integrante:', error);
                res.status(500).json({error: '¡Error al actualizar al integrante!'});
            }
        }
    },



    edit: async function (req, res) {
        const id = parseInt(req.params.id);
        const integrantes = await getAll("SELECT * FROM integrantes WHERE activo = 1 ORDER BY nombre");
        const tipoMedia = await getAll("SELECT * FROM tipoMedia WHERE activo = 1 ORDER BY nombre");
        try {
            const media = await mediaModel.getById(id);
            if (!media) {
                throw new Error('No se encontró el medio de comunicación');
            }

            const integranteSeleccionado = await getOne("SELECT * FROM integrantes WHERE matricula = ?", [media.matricula]);

            res.render("admin/media/mediaEditform", {
                integrantes: integrantes,
                tipoMedia: tipoMedia,
                media: media,
                integranteSeleccionado: integranteSeleccionado
            });
        } catch (err) {
            console.error(err);
            res.redirect("/admin/media/listar?error=" + encodeURIComponent('¡Error al obtener el registro!'));
        }
    },


    destroy: async function (req, res) {
        const id = parseInt(req.params.id);
        try {
            await mediaModel.delete(id);
            res.redirect("/admin/media/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
        } catch (error) {
            console.error('Error al eliminar el Media:', error);
            res.redirect("/admin/media/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
        }
    }
};

module.exports = MediaControllers;