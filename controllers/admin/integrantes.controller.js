/*
index - listado
create - formulario de creación
store - método de guardar en la base de datos
show - formulario de ver un registor
update - método de editar un registro
edit - formulario de edición
destroy - operación de eliminar un registro
*/

const {asociacion} = require("../../db/conexion");
const integranteStoreSchema = require("../../validators/integrante/crearIntegrante");
const editarIntegranteStoreSchema = require("../../validators/integrante/editarIntegrante");
const integranteModel = require("../../models/integrante.model");

const IntegrantesControllers = {

    //Listado de integrantes de /admin/integrantes
    index: async function (req, res) {
        try {
            const integrantes = await integranteModel.getAll(req, res);
            res.render('admin/integrantes/index', {
                integrantes: integrantes
            });
        } catch (error) {
            console.error('Error al obtener los integrantes:', error);
            res.status(500).send('Error al obtener los integrantes');
        }

    },

    create: async function (req, res) {
        res.render("admin/integrantes/crearIntegrante");
    },


    store: async function (req, res) {
        const { error } = integranteStoreSchema.validate(req.body);

        if (error) {
            const mensaje = error.details.map(detail => detail.message);
            const mensajeFormateado = mensaje.join('\n');
            return res.status(400).json({ error: mensajeFormateado });
        }
        try {
            const { activo, matricula, ...campos } = req.body;
            const isActive = activo === 'on' ? 1 : 0;
            const nuevoReq = { ...campos, activo: isActive, matricula };
            const existeIntegrante = await integranteModel.getByField('integrantes', 'matricula', matricula);
            if (existeIntegrante) {
                return res.status(400).json({ error: '¡Ya existe un integrante con la matrícula que has introducido!' });
            }
            await integranteModel.create(nuevoReq);
            res.json({ success: true,message: 'Registro creado correctamente.' });
        } catch (error) {
            console.error('Error al crear el integrante:', error);
            res.status(500).json({ error: 'Error al crear el integrante' });
        }
    },


    update: async function (req, res) {
        const id = parseInt(req.params.id);
        const {error} = editarIntegranteStoreSchema.validate(req.body);
        if (error) {
            const mensaje = error.details.map (detail => detail.message);
            const mensajeFormateado = mensaje.join('\n');
            return res.status(400).json({error:mensajeFormateado});
        }else {
            try {
                await integranteModel.update(req.body, id);
                res.json({success: true, message: 'Registro actualizado correctamente.'});
            } catch (error) {
                console.error('Error al actualizar el integrante:', error);
                res.status(500).json({error: '¡Error al actualizar al integrante!'});
            }
        }
    },

    edit: async function (req, res) {
        const id = parseInt(req.params.id);
        try {
            const integrante = await integranteModel.getById(id);
            res.render('admin/integrantes/editform', {
                integrante: integrante
            });
        } catch (error) {
            console.error('Error al obtener el integrante:', error);
            res.status(500).send('Error al obtener el integrante');
        }
    },

    destroy: async function (req, res) {
        const matricula = req.params.matricula;
        if (await asociacion(matricula)) {
            res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡No se puede eliminar el resgistro, esta información ya esta siendo utilizada en otras entidades!'));
        } else {
            try {
                await integranteModel.delete(matricula);
                res.redirect("/admin/integrantes/listar?success=" + encodeURIComponent('¡Registro eliminado correctamente!'));
            } catch (error) {
                console.error('Error al eliminar el integrante:', error);
                res.redirect("/admin/integrantes/listar?error=" + encodeURIComponent('¡Error al eliminar el registro!'));
            }
        }
    },
};


module.exports = IntegrantesControllers;