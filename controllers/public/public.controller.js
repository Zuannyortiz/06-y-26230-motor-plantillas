const {getAllIntegrantes, getAlltMatriculas, getByMatricula} = require("../../models/integrante.model");
const {getMediaByMatricula} = require("../../models/media.model");
const {getAllTipoMedia} = require("../../models/tipoMedia.model");

const PublicController = {
    index: async (req, res) => {
        const rows = await getAllIntegrantes();
        res.render("index", {
            integrantes: rows,
        });
    },
    curso: async (req, res) => {
        const rows = await getAllIntegrantes();
        res.render("curso",{
            integrantes: rows,
        })
    },
    word_cloud: async (req, res) => {
        const rows = await getAllIntegrantes();
        res.render("word_cloud",{
            integrantes: rows,
        })
    },
    matricula: async (req, res, next) => {
        const matriculas = (await getAlltMatriculas()).map(matricula => matricula.matricula);
        const tipoMedia = await getAllTipoMedia();
        const matricula = req.params.matricula;
        const integrantes = await getAllIntegrantes();

        if (matriculas.includes(matricula)) {
            const integranteFilter = await getByMatricula(matricula);
            const mediaFilter = await getMediaByMatricula(matricula);
            res.render('integrantes', {
                integrante: integranteFilter,
                tipoMedia: tipoMedia,
                media: mediaFilter,
                integrantes: integrantes,
            });
        } else {
            next();
        }
    }

}

module.exports = PublicController;