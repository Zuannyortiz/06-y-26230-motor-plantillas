const loginSchema = require('../../validators/auth/login');
const authModel = require('../../models/auth.model');
const bcrypt = require('bcrypt');

const authController = {
    loginForm: (req, res) => {
        res.render('login/login');
    },

    login:  async (req, res) => {
        const {error} = loginSchema.validate(req.body);
        if (error) {
            const mensaje = error.details.map(detail => detail.message);
            const mensajeFormateado = mensaje.join('\n');
            res.redirect(`/auth/login?errors=${encodeURIComponent(mensajeFormateado)}`);
        }else {
            const {usuario, contrasenha} = req.body;
            try {
                const user = await authModel.getUserByEmail(usuario);
                if (user && await bcrypt.compare(contrasenha, user.contrasenha)) {
                    req.session.userId = user.id;
                    res.redirect('/admin');
                } else {
                    res.redirect('/auth/login?errors=Usuario o contraseña incorrectos');
                }
            } catch (err) {
                res.redirect('/auth/login?errors=Error al iniciar sesión');
            }
        }
    },
    logout: (req, res) => {
        req.session.destroy((err) => {
            if (err) {
                return res.redirect('/');
            }
            res.clearCookie('sid');
            res.redirect('/');
        });
    }

};
module.exports = authController;