const Joi = require("joi");

    const integranteStoreSchema = Joi.object({
        nombre: Joi.string()
            .required()
            .messages({
                "string.empty": "El nombre no puede estar vacío",
                "any.required": "El nombre es requerido",
            }),

        apellido: Joi.string()
            .required()
            .messages({
                "string.empty": "El apellido no puede estar vacío",
                "any.required": "El apellido es requerido",
            }),

        matricula: Joi.string()
            .required()
            .messages({
                "string.empty": "La matricula no puede estar vacía",
                "any.required": "La matricula es requerida",
            }),
        pagina: Joi.string()
            .required()
            .messages({
                "string.empty": "La pagina no puede estar vacía",
                "any.required": "La pagina es requerida",
            }),
        activo: Joi.required()
            .messages({
                "any.required": "El estado es requerido",
            }),
    }).options({abortEarly: false});

module.exports = integranteStoreSchema;