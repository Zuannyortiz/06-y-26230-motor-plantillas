const Joi = require("joi");

const editarMediaStoreSchema = Joi.object({
    tipoMedia: Joi.string()
        .required()
        .messages({
            "string.empty": "El campo tipo media no puede estar vacío",
            "any.required": "El campo tipo media es requerido",
        }),
    titulo: Joi.string()
        .required()
        .messages({
            "string.empty": "El titulo no puede estar vacío",
            "any.required": "El titulo es requerido",
        }),
    integrante: Joi.string()
        .required()
        .messages({
            "string.empty": "El campo integrante no puede estar vacía",
            "any.required": "El campo integrante es requerida",
        }),
    activo: Joi.required()
        .messages({
            "any.required": "El estado es requerido",
        }),
    url: Joi.string().uri().optional().messages({
        "string.uri": "La URL debe ser válida",
        "string.empty": "La URL no puede estar vacía "
    }),
    srcPath: Joi.binary().optional().messages({
        "binary.base": "El SRC debe ser un archivo válido",
    })

}).options({abortEarly: false});

module.exports = editarMediaStoreSchema;