const Joi = require("joi");

const EditarTipoMediaStoreSchema = Joi.object({
    nombre: Joi.string()
        .required()
        .messages({
            "string.empty": "El nombre no puede estar vacío",
            "any.required": "El nombre es requerido",
        }),
    activo: Joi.required()
        .messages({
            "any.required": "El estado es requerido",
        }),
});

module.exports = EditarTipoMediaStoreSchema;