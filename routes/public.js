const express = require ("express");
const router = express.Router();
require("dotenv").config();
const PublicController = require('../controllers/public/public.controller');

router.use((req, res, next) => {
    res.locals.repositorio = process.env.ENLACEGIT;
    res.locals.nombre = process.env.NOMBRE;
    res.locals.materia = process.env.DATOSMATERIA;
    next();
});
router.get("/", PublicController.index);

router.get("/curso", PublicController.curso);

router.get("/word_cloud", PublicController.word_cloud);

router.get("/:matricula", PublicController.matricula);
module.exports = router;
